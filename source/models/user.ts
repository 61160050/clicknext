import mongoose, { Schema } from 'mongoose';
import IUser from '../interfaces/user';
import Account from './account';

const UserSchema: Schema = new Schema(
    {
        username: { type: String, required: true, unique: true },
        password: { type: String, required: true },
        name: { type: String, required: true },
        account: { type: Account }
    },
    {
        timestamps: true
    }
);

export default mongoose.model<IUser>('User', UserSchema);
