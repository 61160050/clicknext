import mongoose, { Schema } from 'mongoose';
import Account from '../interfaces/account';
import Histories from './histories';

const AccountSchema: Schema = new Schema({
    name: { type: String, required: true },
    SerialNumber: { type: String, required: true, unique: true },
    balance: { type: Number, required: true },
    histories: { type: Histories }
});

export default mongoose.model<Account>('Account', AccountSchema);
