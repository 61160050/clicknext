import mongoose, { Schema } from 'mongoose';
import Histories from '../interfaces/histories';

const HistoriesSchema: Schema = new Schema({
    forName: { type: String, required: true },
    formSerialNumber: { type: String, required: true },
    toName: { type: String, required: true },
    toSerialNumbe: { type: String, required: true },
    amount: { type: Number, required: true },
    DateTranfer: { type: Date, require: true }
});

export default mongoose.model<Histories>('Histories', HistoriesSchema);
