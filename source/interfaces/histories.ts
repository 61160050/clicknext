import { Document } from 'mongoose';

export default interface Histories extends Document {
    formName: string;
    formSerialNumber: string;
    toName: string;
    toSerialNumber: string;
    amount: Number;
    dateTranfer: {
        type: Date;
        default: Date;
    };
}
