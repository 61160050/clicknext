import { Document } from 'mongoose';
import Histories from './histories';

export default interface Account extends Document, Histories {
    name: string;
    SerialNumber: string;
    balance: Number;
    histories?: [Histories];
}
