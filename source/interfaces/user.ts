import { Document } from 'mongoose';
import account from './account';

export default interface IUser extends Document, account {
    username: string;
    password: string;
    name: string;
    account?: [account];
}
